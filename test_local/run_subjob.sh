#!/bin/bash
/homes/oakland/jfroula/KBASE/kb_Bowtie2/test_local/run_docker.sh run --rm --user $(id -u) -v /homes/oakland/jfroula/KBASE/kb_Bowtie2/test_local/subjobs/$1/workdir:/kb/module/work -v /homes/oakland/jfroula/KBASE/kb_Bowtie2/test_local/workdir/tmp:/kb/module/work/tmp $4 -e "SDK_CALLBACK_URL=$3" $2 async
